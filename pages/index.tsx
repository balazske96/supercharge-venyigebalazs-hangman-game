import type {NextPage} from 'next'
import styles from '../styles/Home.module.scss'
import {HangmanDisplay} from "../components/HangmanDisplay";
import {DisplayWord} from "../components/WordDisplay";
import {AvailableLettersContainer} from "../components/AvailableLettersContainer";
import {Button} from "../components/Button";
import {ButtonType} from "../components/Button/ButtonType";
import useGameContext from "../hooks/useGameContext";
import {EndModal} from "../components/EndModal";
import {EndModalResult} from "../components/EndModal/EndModalResult";
import {Instructions} from "../components/Instructions";

const Home: NextPage = () => {

    const {
        numberOfWrongAnswers,
        word,
        isWin,
        newGame,
        endGame,
        gameOver,
        instructionsAccepted,
    } = useGameContext()

    return (
        <div className={styles.container}>
            <div className={styles.hangmanDisplayContainer}>
                <HangmanDisplay numberOfWrongAnswers={numberOfWrongAnswers!}/>
            </div>
            <div className={styles.playgroundContainer}>
                <h1>The Hangman</h1>
                <DisplayWord word={word!}/>
                <p>Play with a word</p>
                <AvailableLettersContainer/>
            </div>
            <div className={styles.actionContainer}>
                <Button label={'END GAME'} onClick={() => endGame!()} type={ButtonType.WHITE}/>
                <Button label={'START NEW GAME'} onClick={() => newGame!()} type={ButtonType.BLACK}/>
            </div>
            {isWin && !gameOver && <EndModal result={EndModalResult.WIN}/>}
            {!isWin && gameOver && <EndModal result={EndModalResult.LOSE}/>}
            {!instructionsAccepted && <Instructions/>}
        </div>
    )
}

export default Home
