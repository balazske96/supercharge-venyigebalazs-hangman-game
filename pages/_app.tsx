import '../styles/globals.scss'
import type {AppProps} from 'next/app'
import {GameContextProvider} from "../hooks/useGameContext";

function MyApp({Component, pageProps}: AppProps) {
    return (
        <GameContextProvider>
            <Component {...pageProps} />
        </GameContextProvider>
    )
}

export default MyApp
