import ButtonProps from "./ButtonProps";
import styles from './style.module.scss'
import {ButtonType} from "./ButtonType";
import clsx from "clsx";

export default function Button({label, onClick, type = ButtonType.BLACK}: ButtonProps) {

    const computedClassName = clsx({
        [styles.container]: true,
        [styles.black]: type === ButtonType.BLACK,
        [styles.white]: type === ButtonType.WHITE
    })

    return (
        <button className={computedClassName} onClick={onClick}>
            {label}
        </button>
    )
}