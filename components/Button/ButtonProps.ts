import {ButtonType} from "./ButtonType";

export default interface ButtonProps {
    label: string
    onClick: () => void
    type?: ButtonType 
}