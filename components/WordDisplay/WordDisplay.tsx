import WordDisplayProps from "./WordDisplayProps";
import styles from './styles.module.scss'
import {Letter} from "../Letter";
import useGameContext from "../../hooks/useGameContext";
import {useCallback} from "react";

export default function WordDisplay({word}: WordDisplayProps) {

    const {correctLetters} = useGameContext()

    const displayLetter = useCallback((letter: string) => {
        return correctLetters?.includes(letter)
    }, [correctLetters])

    return (
        <div className={styles.wordDisplayContainer}>
            {Array.from(word).map((letter, index) => (
                <Letter key={index} letter={letter} displayLetter={displayLetter(letter)}/>
            ))}
        </div>
    )
}