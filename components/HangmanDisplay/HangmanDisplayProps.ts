export default interface HangmanDisplayProps {
    numberOfWrongAnswers: number
}