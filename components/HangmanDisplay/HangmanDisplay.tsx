import styles from './styles.module.scss'
import clsx from "clsx";
import HangmanDisplayProps from "./HangmanDisplayProps";

export default function HangmanDisplay({numberOfWrongAnswers}: HangmanDisplayProps) {

    const computedClassName = clsx({
        [styles.container]: true,
        [styles.clearGame]: numberOfWrongAnswers === 0,
        [styles.firstWrongAnswer]: numberOfWrongAnswers === 1,
        [styles.secondWrongAnswer]: numberOfWrongAnswers === 2,
        [styles.thirdWrongAnswer]: numberOfWrongAnswers === 3,
        [styles.fourthWrongAnswer]: numberOfWrongAnswers === 4,
        [styles.fifthWrongAnswer]: numberOfWrongAnswers === 5,
    })

    return (
        <div className={computedClassName}>
            <svg viewBox="0 0 10 12">
                <path d="M1,11 h8"/>
                <path d="M9,11 v-10"/>
                <path d="M9,1 h-4"/>
                <path d="M5,1 v2"/>
                <circle cx="5" cy="4" r="1" id="head"/>
                <path d="M5,5 v3" id="body"/>
                <path d="M5,5 l-2,2" id="left-arm"/>
                <path d="M5,5 l2,2" id="right-arm"/>
                <path d="M5,8 l-2,2" id="left-leg"/>
                <path d="M5,8 l2,2" id="right-leg"/>
            </svg>
        </div>
    )
}