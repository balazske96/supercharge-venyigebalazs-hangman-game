import styles from './styles.module.scss'
import {Button} from "../Button";
import useGameContext from "../../hooks/useGameContext";
import EndModalProps from "./EndModalProps";
import {EndModalResult} from "./EndModalResult";

export default function EndModal({result}: EndModalProps) {

    const {newGame} = useGameContext()

    return (
        <div className={styles.container}>
            <div className={styles.modalContentBody}>
                <h1>
                    {result === EndModalResult.WIN ? 'Congratulation!' : 'Game Over!'}
                </h1>
                <p>
                    {result === EndModalResult.WIN ? 'You guessed all letters correctly.' : 'Try again!'}
                </p>
                <Button label={'NEW GAME'} onClick={() => newGame!()}/>
            </div>
        </div>
    )
}