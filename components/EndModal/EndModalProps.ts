import {EndModalResult} from "./EndModalResult";

export default interface  EndModalProps {
    result: EndModalResult
}