export enum EndModalResult {
    WIN = 'WIN',
    LOSE = 'LOSE'
}