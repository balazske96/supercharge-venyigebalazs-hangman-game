import styles from './styles.module.scss'
import {Button} from "../Button";
import useGameContext from "../../hooks/useGameContext";
import {ButtonType} from "../Button/ButtonType";

export default function Instructions() {

    const {acceptInstructions} = useGameContext()

    return (
        <div className={styles.container}>
            <div className={styles.instructionsBody}>
                <svg viewBox="0 0 10 12">
                    <path d="M1,11 h8"/>
                    <path d="M9,11 v-10"/>
                    <path d="M9,1 h-4"/>
                    <path d="M5,1 v2"/>
                    <circle cx="5" cy="4" r="1" id="head"/>
                    <path d="M5,5 v3" id="body"/>
                    <path d="M5,5 l-2,2" id="left-arm"/>
                    <path d="M5,5 l2,2" id="right-arm"/>
                    <path d="M5,8 l-2,2" id="left-leg"/>
                    <path d="M5,8 l2,2" id="right-leg"/>
                </svg>
                <h2>Game instructions</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet augue consectetur, pulvinar
                    enim non, hendrerit magna. Etiam eleifend, augue nec aliquam tincidunt, lorem velit egestas nibh,
                    quis tristique odio ex id arcu. Cras maximus dapibus mauris eget vulputate
                </p>
                <Button label={'GOT IT!'} onClick={() => acceptInstructions!()} type={ButtonType.BLACK}/>
            </div>
        </div>
    )
}