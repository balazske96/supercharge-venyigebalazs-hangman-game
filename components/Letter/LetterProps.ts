export default interface LetterProps {
    letter: string
    displayLetter?: boolean
    onClick?: () => void
    disabled?: boolean
}