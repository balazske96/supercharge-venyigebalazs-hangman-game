import LetterProps from "./LetterProps";
import clsx from "clsx";
import styles from './styles.module.scss'


export default function Letter({letter, onClick, disabled = false, displayLetter = true}: LetterProps) {

    const computedClassName = clsx({
        [styles.letterContainer]: true,
        [styles.displayLetter]: displayLetter,
        [styles.disabled]: disabled
    })

    return (
        <div
            className={computedClassName}
            onClick={() => {
                if (!disabled && !!onClick) onClick()
            }}>
            {letter}
        </div>
    )
}