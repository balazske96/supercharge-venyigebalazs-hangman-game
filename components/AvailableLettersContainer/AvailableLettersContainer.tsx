import {Letter} from "../Letter";
import styles from './styles.module.scss'
import available_letters from '../../data/available_letters.json'
import {useCallback, useEffect} from "react";
import useGameContext from "../../hooks/useGameContext";

export default function AvailableLettersContainer() {

    const {usedLetters, guess} = useGameContext()

    const isDisabled = useCallback((letter: string) => {
        return usedLetters?.includes(letter)
    }, [usedLetters])

    return (
        <div className={styles.container}>
            {available_letters.map((letter, index) => (
                    <Letter
                        disabled={isDisabled(letter)}
                        // @ts-ignore
                        onClick={() => guess(letter)}
                        key={index} letter={letter}
                        displayLetter={true}
                    />
                )
            )}
        </div>
    )
}