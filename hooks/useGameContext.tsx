import React, {useEffect, useMemo, useState} from 'react';
import words_data from '../data/hangman_words.json'
import {is} from "immutable";


interface GameContextProviderProps {
    children: React.ReactNode
}

interface GameContextValues {
    word?: string
    numberOfWrongAnswers?: number
    guess?: (letter: string) => void
    usedLetters?: string[]
    correctLetters?: string[]
    gameOver?: boolean
    isWin?: boolean
    newGame?: () => void
    instructionsAccepted?: boolean
    acceptInstructions?: () => void
    endGame?: () => void
}

const GameContext = React.createContext<GameContextValues>({})

export function GameContextProvider({children}: GameContextProviderProps) {

    const [usedLetters, setUsedLetters] = useState<string[]>([])
    const [correctLetters, setCorrectLetters] = useState<string[]>([])
    const [numberOfWrongAnswers, setNumberOfWrongAnswers] = useState<number>(0)
    const [isWin, setIsWin] = useState<boolean>(false)
    const [isGameOver, setIsGameOver] = useState<boolean>(false)
    const [instructionsAccepted, setInstructionsAccepted] = useState<boolean>(false)
    const [flagToCreateNewWord, setFlagToCreateNewWord] = useState<boolean>(false)

    const getRandomWord = () => {
        return words_data[Math.floor(Math.random() * words_data.length)].toUpperCase()
    }

    const word = useMemo(() => getRandomWord(), [flagToCreateNewWord]);

    const wordContainerTheLetter = (letter: string) => {
        return word.toUpperCase().includes(letter.toUpperCase())
    }

    const guess = (letter: string) => {
        setUsedLetters(prevState => [...prevState, letter])

        if (wordContainerTheLetter(letter)) {
            setCorrectLetters(prevState => [...prevState, letter])
        } else {
            setNumberOfWrongAnswers(prevState => prevState + 1)
        }
    }

    const acceptInstructions = () => {
        setInstructionsAccepted(true)
    }

    const endGame = () => {
        setIsGameOver(true)
    }

    const newGame = () => {
        setUsedLetters([])
        setCorrectLetters([])
        setNumberOfWrongAnswers(0)
        setIsGameOver(false)
        setIsWin(false)
        setFlagToCreateNewWord(prevState => !prevState)
    }

    useEffect(() => {
        if (numberOfWrongAnswers >= 6) {
            setIsGameOver(true)
        }
    }, [numberOfWrongAnswers])

    useEffect(() => {
        if (!isGameOver && Array.from(word).every(letter => correctLetters.includes(letter))) setIsWin(true)
    }, [correctLetters, word])

    return (
        <GameContext.Provider
            value={{
                word: word,
                numberOfWrongAnswers: numberOfWrongAnswers,
                usedLetters: usedLetters,
                correctLetters: correctLetters,
                gameOver: isGameOver,
                isWin: isWin,
                instructionsAccepted: instructionsAccepted,
                newGame: newGame,
                guess: guess,
                acceptInstructions: acceptInstructions,
                endGame: endGame

            }}>
            {children}
        </GameContext.Provider>
    )
}

export default function useGameContext() {
    return React.useContext<GameContextValues>(GameContext)
}